import React from 'react';

const AuthContext = React.createContext({
    Authenticated: "no"
});

export default AuthContext;