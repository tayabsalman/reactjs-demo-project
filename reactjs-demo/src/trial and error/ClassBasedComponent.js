import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons : [
      {name: "Tayab", age: 24},
      {name: "Salman", age : 20},
      {name : "Tayabsalman", age : 25}
    ]
  }

  SwitchNameHandler = () => {
    this.state.persons[0].name = "Max";
    this.state.persons[2].age = 100;
    console.log(this.state);
    this.setState(this.state);
    console.log(this.state);
  }

  render() {
    return (
      <div className="App">
        <h1> Hi, this is react App</h1>
        <p>This is really working</p>
        <button onClick = {this.SwitchNameHandler}>Switch Name</button>
        <Person age = { this.state.persons[0].age } name = { this.state.persons[0].name }/>
        <Person age = { this.state.persons[1].age } name = { this.state.persons[1].name }/>
        <Person age = { this.state.persons[2].age } name = { this.state.persons[2].name }/>
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Hi, this is react App'));
  }
}

export default App;
