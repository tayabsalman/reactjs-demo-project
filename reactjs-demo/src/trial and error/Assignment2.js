import React, { useState } from 'react';
import "./App.css";
import Validation from "./Assignment2/Validation/Validation";
import Character from "./Assignment2/Character/Character";

const App = () => {
  const [state, setState] = useState({userInput: ''});

  const InputValueHandler = event => {
    const currentState = {...state};
    currentState.userInput = event.target.value;
    setState(currentState);
  };

  const CharacterClickHandler = index => {
    let currentState = {...state};
    const text = currentState.userInput.split("");
    text.splice(index, 1);
    currentState.userInput = text.join("");
    setState(currentState);
  };

  const characters = state.userInput.split("").map((character, index) => {
    return <Character 
            key = {index}
            click= {() => CharacterClickHandler(index)}
            text= {character}
          />
  });
  return (
    <div className="App">
      <h2>Assignment No.2</h2>
      <h3>Upon entering a string into input field</h3>
      <p>Displaying length of the entered string</p>
      <p>validating length of the entered string</p>
      <p>showing entered characters seperately, upon clicking of which the clicked character will get removed from the entered string</p>
      <input 
      onChange={InputValueHandler} 
      value={state.userInput} />
      <p>Entered string length: {state.userInput.length} </p>
      <Validation stringLength={state.userInput.length}/>
      <div>
        {characters}
      </div>
    </div>
  );
}

export default App;