import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

const App = props => {
  const [AppState, SetAppState] = useState(
    {
      persons : [
        {name: "Tayab", age: 24, id:1},
        {name: "Salman", age : 20, id:2},
        {name : "Tayabsalman", age : 25, id:3}
      ],
      showPersons: false
    }
  );

  const TogglePersons = () => {
    const currentState = {...AppState};
    currentState.showPersons = !currentState.showPersons;
    SetAppState(currentState);
  }

  const NameChangeHandler = (event, id) => {
    const personIndex = AppState.persons.findIndex(person => {
      return person.id === id;
    });
    const currentState = {...AppState};
    currentState.persons[personIndex].name = event.target.value;
    SetAppState(currentState);
  }

  const DeletePersonHandler = (id) => {
    const personIndex = AppState.persons.findIndex(person => {
      return person.id === id;
    });
    const currentState = {...AppState};
    currentState.persons.splice(personIndex, 1);
    SetAppState(currentState);
  }

  let persons = null;
  if(AppState.showPersons){
    persons = (
      <div>
        {
          AppState.persons.map(person => {
            return <Person 
              key = {person.id}
              name = {person.name} 
              age = {person.age} 
              changed = {(event) => NameChangeHandler(event, person.id)}
              click = {() => DeletePersonHandler(person.id)}> Text which goes as children </Person>
          })
        }
      </div>
      );
  }


  return (
    <div className="App">
      <h1> Hi, this is react App</h1>
      <p>This is really working</p>
      <button onClick = {TogglePersons}>Toggle Persons</button>
      {persons}
    </div>    
  );
  }

  export default App;