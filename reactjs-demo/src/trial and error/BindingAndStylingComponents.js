import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

const App = props => {
  const [AppState, SetAppState] = useState(
    {
      persons : [
        {name: "Tayab", age: 24},
        {name: "Salman", age : 20},
        {name : "Tayabsalman", age : 25}
      ],
      otherState: "Some other state"
    }
  );
  const SwitchNameHandler = (newName) => {
    AppState.persons[0].name = newName;
    AppState.persons[2].age = 100;
    const newstate = {...AppState}
    //for some reason if I modify currenstate and pass the same, the state was updating but not he in UI
    //it worked when I created a new similar object and passed.
    SetAppState(newstate);
  };
  const NameChangedHandler = (event) => {
    AppState.persons[1].name = event.target.value;
    SetAppState({...AppState});
  };

  const buttonStyle = {
    "background-color":"white",
    border: "1px solid #cccc",
    "padding":"5px"
  };
  return (
    <div className="App">
      <h1> Hi, this is react App</h1>
      <p>This is really working</p>
      <button style={buttonStyle}
      onClick = {SwitchNameHandler.bind(this, "new name")}>Switch Name</button>
      <Person 
        age = { AppState.persons[0].age } name = { AppState.persons[0].name }/>
      <Person 
        age = { AppState.persons[1].age } name = { AppState.persons[1].name } 
        click = {() => SwitchNameHandler("old name")} 
        changed = {NameChangedHandler}/>
      <Person 
        age = { AppState.persons[2].age } name = { AppState.persons[2].name }/>
    </div>    
  );
}
export default App;
