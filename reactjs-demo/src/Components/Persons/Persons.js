import React, {Component} from 'react';
import Person from './Person/Person';

class Persons extends Component {
    constructor(props){
        super(props);
    }
    
    componentDidMount() {
        console.log("Persons.js: Component did mount");
    }

    componentDidUpdate(){
        console.log("Persons.js: Component did update");
    }
    componentWillUnmount(){
        console.log("Persons.js: component will unmount");
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log("Persons.js: should component update");
        return this.props !== nextProps;
    }
    
    render() {
        console.log("Persons.js: Redering");
        return this.props.persons.map(person => {
            return <Person 
                key = {person.id}
                name = {person.name} 
                age = {person.age} 
                changed = {(event) => this.props.changed(event, person.id)}
                click = {() => this.props.clicked(person.id)}> Text which goes as children </Person>
            });
    }
}

export default Persons;