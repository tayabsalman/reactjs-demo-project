import React, {useEffect, useRef} from 'react';

import AuthContext from '../../../context/AuthContext';

import Aux from '../../../hoc/Aux';
import withClass from '../../../hoc/withClass';

import classes from "./Person.css";

const Person = (props) => {
    console.log("Person.js: Redering");
    const inputRef = useRef(null);
    useEffect(() => {
        console.log("Person.js: DOM created/Update");
        inputRef.current.focus();
    },[props.name]);
    return (
        <Aux>
            <p className={classes.PersonName} onClick = {props.click}> I'm { props.name }, i'm { props.age } years old</p>
            <p> { props.children }</p>
            <input ref={inputRef} onChange={props.changed} value={props.name}></input>
            <AuthContext.Consumer>
                {(context) => {
                    return <p> Authenticated: {context.Authenticated}</p>
                }}
            </AuthContext.Consumer>
        </Aux>
    );
};

export default withClass(Person, classes.Person);