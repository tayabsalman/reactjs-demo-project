import React, {useEffect, memo} from 'react';
import classes from '../../Containers/App/App.css';

const Cockpit = (props) => {

    const buttonClasses = [classes.Button];
    if(props.showPersons)
        buttonClasses.push(classes.Red);

    const classStyling = [];
    if(props.personsLength <=2)
      classStyling.push(classes.red);
    if(props.personsLength <= 1)
      classStyling.push(classes.bold);
    
    useEffect(() => {
      console.log("Cockpit.js: Redering");
    }, [props.showPersons, props.personsLength]);

    console.log("Cockpit.js: Redering outside");
    return (
        <div>
            <h1> Hi, this is react App</h1>
            <p className={classStyling.join(" ")}>This is really working</p>
            <button 
            onClick = {props.clicked} 
            className={buttonClasses.join(" ")} >Toggle Persons</button>
        </div>
    );
};

export default memo(Cockpit);