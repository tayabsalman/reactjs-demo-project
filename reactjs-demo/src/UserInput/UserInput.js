import React from 'react';

const UserInput = (props) => {
    return (
        <div className="Person" style={props.style}>
            <input onChange = {props.changed} value={props.value}/>
        </div>
    );
};

export default UserInput;