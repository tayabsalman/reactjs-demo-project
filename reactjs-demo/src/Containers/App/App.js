import React, { Component } from 'react';

import AuthContext from '../../context/AuthContext'

import Aux from '../../hoc/Aux';
import withClass from '../../hoc/withClass';

import Cockpit from '../../Components/Cockpit/Cockpit';
import Persons from '../../Components/Persons/Persons'
import classes from './App.css';

class App extends Component {
  constructor(){
    console.log("App.js: Contructing");
    super();
    this.state = {
      persons : [
        {name: "Tayab", age: 24, id:1},
        {name: "Salman", age : 20, id:2},
        {name : "Tayabsalman", age : 25, id:3}
      ],
      showPersons: false
    }
  }

  TogglePersons = () => {
    // const currentState = {...this.state};
    this.setState((prevState, props) => {
      prevState.showPersons = !prevState.showPersons;
      return prevState; 
    });
  }

  NameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(person => {
      return person.id === id;
    });
    const currentState = {...this.state};
    currentState.persons[personIndex].name = event.target.value;
    this.setState(currentState);
  }

  DeletePersonHandler = (id) => {
    const personIndex = this.state.persons.findIndex(person => {
      return person.id === id;
    });
    const currentState = {...this.state};
    currentState.persons.splice(personIndex, 1);
    this.setState(currentState);
  }

  componentDidMount() {
    console.log("App.js: Component did mount");
  }

  componentDidUpdate(){
    console.log("App.js: Component did update");
  }
  componentWillUnmount(){
    console.log("App.js: component will unmount");
  }

  render() {
    console.log("App.js: Redering");
    let persons = null;

    if(this.state.showPersons){
      persons = (
        <Persons 
        persons = {this.state.persons} 
        changed={this.NameChangeHandler} 
        clicked={this.DeletePersonHandler}/>
      );
    }

    return (
      <Aux>
        <Cockpit 
        showPersons={this.state.showPersons} 
        personsLength={this.state.persons.length} 
        clicked={this.TogglePersons}/>
        <AuthContext.Provider value={{Authenticated: "yes"}}>
          {persons}
        </AuthContext.Provider>
      </Aux>
      );
    }
  }

  export default withClass(App, classes.App);
