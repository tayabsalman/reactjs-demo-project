import React from 'react';

const Validation = props => {
    let validationText = (props.stringLength < 5)?"Text too short...":(props.stringLength > 10)?"Text too long...":"";
    
    return (
        <div>
            {validationText}
        </div>
    );
};

export default Validation;