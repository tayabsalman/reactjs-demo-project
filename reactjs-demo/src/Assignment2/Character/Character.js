import React from 'react';
import "./Character.css";

const Character = props => {
    return (
        <div className="Character" onClick={props.click}>
            <p>{props.text}</p>
        </div>
    );
};

export default Character;