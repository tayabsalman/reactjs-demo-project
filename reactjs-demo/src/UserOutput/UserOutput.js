import React from "react"
import "../Person/Person.css";
const UserOutput = (props) => {
    return (
        <div className="Person">
            <p> {props.username} </p>
            <p> {props.text}</p>
        </div>
    );
};

export default UserOutput;