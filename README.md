# Demo project with ReactJS
A ReactJS project to understand how react works.
## Installing scripts for creating react app
```
sudo npm install create-react-app -g
```
## Creating a new react js project called reactjs-demo
```
create-react-app reactjs-demo --scripts-version 1.1.5 # scripts-version to get a specific file structure
```

### In order to check this demo in action, do the following
0. Make sure you have npm and node installed
1. Clone this repo.
```
git clone https://gitlab.com/tayabsalman/reactjs-demo-project.git
```
2. Go inside reactjs-demo folder
```
 cd reactjs-demo-project/reactjs-demo
```
3. Run the following
```
npm install
npm start
```

## Built With

* [NodeJS](https://nodejs.org) - (version: 13.9.0) An open source JavaScript server environment
* [npm](https://www.npmjs.com/package/npm) - (version: 6.13.7) A JavaScript package manager
* [create-react-app](https://create-react-app.dev) - (version: 3.4.1) Out of the box setup for React Applications


### (This project has been concluded)